import { Component, ViewChild, ElementRef } from "@angular/core";
import { NavController } from "ionic-angular";
import { Geolocation } from "@ionic-native/geolocation";
import "rxjs/add/operator/map";
import { Http } from "@angular/http";

declare var google;

@Component({
  selector: "home-page",
  templateUrl: "home.html"
})
export class HomePage {
  @ViewChild("map") mapElement: ElementRef;
  @ViewChild("directionsPanel") directionsPanel: ElementRef;
  map: any;
  data: any;
  directionsService = new google.maps.DirectionsService();
  directionsDisplay = new google.maps.DirectionsRenderer();

  constructor(
    public navCtrl: NavController,
    public geolocation: Geolocation,
    public http: Http
  ) {}

  ionViewDidLoad() {
    this.loadMap();
    this.startNavigating();
  }

  loadMap() {
    this.geolocation.getCurrentPosition().then(
      position => {
        let latLng = new google.maps.LatLng(
          position.coords.latitude,
          position.coords.longitude
        );

        let mapOptions = {
          center: latLng,
          zoom: 15,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        this.map = new google.maps.Map(
          this.mapElement.nativeElement,
          mapOptions
        );
      },
      err => {
        console.log(err);
      }
    );
  }
  addInfoWindow(marker, content, x, y) {
    this.directionsDisplay.setMap(this.map);
    this.directionsDisplay.setPanel(this.directionsPanel.nativeElement);

    let infoWindow = new google.maps.InfoWindow({
      content: content
    });

    google.maps.event.addListener(marker, "click", () => {
      infoWindow.open(this.map, marker);

      this.geolocation.getCurrentPosition().then(position => {
        this.directionsService.route(
          {
            origin: {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            },
            destination: marker.position,
            travelMode: google.maps.TravelMode["WALKING"]
          },
          (res, status) => {
            if (status == google.maps.DirectionsStatus.OK) {
              this.directionsDisplay.setDirections(res);
            } else {
              console.warn(status);
            }
          }
        );
      });
    });
  }

  addMarkerWithPosition(x, y, content) {
    let marker = new google.maps.Marker({
      map: this.map,
      animation: google.maps.Animation.DROP,
      position: new google.maps.LatLng(x, y)
    });
    this.addInfoWindow(marker, content, x, y);
  }

  startNavigating() {
    this.directionsDisplay.setMap(this.map);
    this.directionsDisplay.setPanel(this.directionsPanel.nativeElement);

    this.geolocation.getCurrentPosition().then(position => {
      this.http
        .get(
          "https://opendata.paris.fr/api/records/1.0/search/?dataset=espaces_verts&rows=10000&facet=type_ev&facet=id_division&facet=adresse_codepostal&facet=ouvert_ferme&facet=id_atelier_horticole&facet=competence&facet=categorie&facet=presence_cloture&facet=proprietaire&facet=gestionnnaire&refine.categorie=Espace+Vert"
        )
        .map(res => res.json())
        .subscribe(data => {
          data.records.map(record => {
            let lat = record.geometry.coordinates[0];
            let long = record.geometry.coordinates[1];
            this.addMarkerWithPosition(long, lat, record.fields.nom_ev);
          });
        });
    });
  }
}
